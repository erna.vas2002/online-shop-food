function render() {

  const productsStore = localStorageUtil.getProduct();
  headerPage.render(productsStore.length);

  productPage.render();
}

let CATALOG = [];


fetch('./server/catalog.json')
  .then(res => res.json())
  .then(body => {
    CATALOG = body;
    setTimeout(() => {
      render();
      spinnerPage.cleareSpinner();
    }, 1000)
    spinnerPage.render();
  })
  .catch(error => {
    errorPage.render();
  })