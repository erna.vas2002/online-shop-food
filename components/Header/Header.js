class Header {

  openCart() {
    cartPage.render();
  }

  render(count) {
    const html = `
      <div class="header-container">
        <h1 class="header-title">
          Product catalog
        </h1>
        <div class="header-counter" onclick="headerPage.openCart()">
          <img src="./components/Header/img/cart.png">
          <p>${count}</p>
        </div>
      </div>
    `

    ROOT_HEADER.innerHTML = html;
  }
}

const headerPage = new Header();
