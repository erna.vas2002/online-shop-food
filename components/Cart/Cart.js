class Cart {
  closeCart(){
    ROOT_CART.innerHTML = '';
  }

  render(){
    let htmlCatalog = '';
    const productStore = localStorageUtil.getProduct();
    let sumPrice = 0;

    CATALOG.forEach(({id, name, price}) => {
      if (productStore.indexOf(id) !== -1) {
        sumPrice += parseInt(price); 
        htmlCatalog += `
        <tr>
            <td class="shopping-element__name">${name}</td>
            <td class="shopping-element__price">${price} р.</td>
          </tr>
        `;
      }
      return sumPrice;
    });
    let html = `
      <div class="shopping-container">
      <div class="shopping__close" onclick="cartPage.closeCart()"></div>
      <h1>Ваш заказ:</h1>
        <table>
          ${htmlCatalog}
        </table>
        <div class="shopping-element__sum-price">
          <p>Сумма заказа:</p>
          <span>${sumPrice} p.</span>
        </div>
      </div>
    `;
    ROOT_CART.innerHTML = html;
  }

}

const cartPage = new Cart();
