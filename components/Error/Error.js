class Error {

  render() {
    let html = `
      <div class="error-container">
        <div class="error__message">
          <h2>Нет доступа!</h2>
          <p>Попробуйте снова!</p>
        </div>
      </div>
    `;

    ROOT_ERROR.innerHTML = html;
  }
}
const errorPage = new Error();
