class Products {
  constructor(){
    this.LabelAdd = 'Добавить в корзину';
    this.LabelRemove = 'Удалить из корзины';
    this.activeClass = 'products-element__btn_active';
  }

  handleSetLocationStorage(element, id) {
    const { pushProduct, products } = localStorageUtil.putProduct(id);
    if(pushProduct){
      element.classList.add(this.activeClass);
      element.innerHTML = this.LabelRemove
    }else{
      element.classList.remove(this.activeClass);
      element.innerHTML = this.LabelAdd
    }
    headerPage.render(products.length);
  }

  render() {
    let productsStore = localStorageUtil.getProduct();
    let htmlCatalog = '';

    CATALOG.forEach(({id, name, price, img}) => {
      let activeText = '';
      let activeClass = '';

       if(productsStore.indexOf(id) === -1){
        activeText = this.LabelAdd;
        
       }else{
        activeText = this.LabelRemove;
        activeClass = ' '+this.activeClass;
       }

      htmlCatalog += `  
        <li class="products-element">
          <img class="products-element__img" src="${img}">
          <span class="products-element__name">${name}</span>
          <span class="products-element__price">${price} р.</span>
          <button class="products-element__btn${activeClass}" onclick="productPage.handleSetLocationStorage(this, '${id}')">${activeText}</button>
        </li>
      `;
    })
    const html = `
      <ul class="products-container">
        ${htmlCatalog}
      </ul>
    `;

    ROOT_PRODUCTS.innerHTML = html;
  }
}

const productPage = new Products();